using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Service.Bot
{
    public class Worker : IHostedService
    {
        private readonly ILogger<Worker> _logger;
        private readonly string URL_API;
        private readonly IConfiguration _config;
        public Worker(ILogger<Worker> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
            URL_API = config.GetSection("ApiHost:Api").Value;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                    loop();
                    await Task.Delay(300000, cancellationToken);
                }
                catch (Exception ex)
                {
                    _logger.LogInformation("Error: " + ex.Message);
                }
            }
        }



        private void loop()
        {
            try
            {
                bool IsLoop = true;
                while (IsLoop)
                {
                    var response = CallForwardApi(URL_API, Method.GET, null, null);
                    if (string.IsNullOrEmpty(response.Content))
                    {
                        return;
                    }
                    else
                    {
                        var data = JsonConvert.DeserializeObject<ServerJson>(Convert.ToString(response.Content));
                        IRestResponse responseHTML = null;
                        if (!string.IsNullOrEmpty(data.method) && data.method.ToLower() == "post")
                        {
                            responseHTML = CallForwardApi(data.url, Method.POST, data.header, data.dataPost);
                        }
                        else
                        {
                            responseHTML = CallForwardApi(data.url, Method.GET, data.header, null);

                        }
                        if (responseHTML.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var contenSend = responseHTML.Content;
                            if (data.base64)
                            {
                                contenSend = Base64Encode(contenSend);
                            }
                            var responseData = CallForwardApi(data.returnUrl, Method.POST, null, new DataSend() { id = data.id, content = responseHTML.Content, header = HeaderToJson(responseHTML.Headers.ToList()) });
                            if (string.IsNullOrEmpty(response.Content))
                            {
                                return;
                            }
                            else
                            {
                                IsLoop = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }
        private string HeaderToJson(List<Parameter> dict) => "{" + string.Join(",", dict.Select(d => string.Format("\"{0}\": \"{1}\"", d.Name, string.Join(",", d.Value)))) + "}";
        public  string Base64Encode(string plainText) => Convert.ToBase64String(Encoding.UTF8.GetBytes(plainText));
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Worker was stopped at: {time}", DateTimeOffset.Now);
            throw new NotImplementedException();
        }

        private IRestResponse CallForwardApi(string apiUrl,Method method, Dictionary<string, string> dataHeader = null , object dataObject = null)
        {
            RestClient client = new RestClient
            {
                BaseUrl = new Uri($"{apiUrl}")
            };

            RestRequest request = new RestRequest($"{apiUrl}", method) { RequestFormat = DataFormat.Json };
            if (dataHeader != null)
            {
                foreach (var item in dataHeader)
                {
                    request.AddHeader($"{item.Key}",item.Value);
                }
            }
            if (dataObject != null)
            {
                request.AddBody(dataObject);
            }
            var response = client.Execute(request);

            return response;
        }
    }
    public class ServerJson
    {
        public string id { get; set; }
        public string url { get; set; }
        public Dictionary<string, string> header { get; set; }
        public string returnUrl { get; set; }
        public string method { get; set; }
        public DataPost dataPost { get; set; }
        public bool base64 { get; set; }
    }
    public class DataSend
    {
        public string id { get; set; }
        public string content { get; set; }
        public string header { get; set; }
    }
    public class Header
    {
        [JsonProperty("user-agent")]
        public string UserAgent { get; set; }
        public string cookie { get; set; }
    }

    public class DataPost
    {
        public string email { get; set; }
        public string password { get; set; }
    }
}
